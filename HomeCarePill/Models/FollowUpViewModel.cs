﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace HomeCarePill.Models
{
    public class FollowUpViewModel
    {
        [Required]
        [Display(Name = "StartDate:")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public string StartDate { get; set; }

        [Required]
        [Display(Name = "EndDate:")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public string EndDate { get; set; }

        [Required]
        [Display(Name = "Drug:")]
        public ICollection<SelectListItem> Drugs { get; set; }

        public string SelectedDrug { get; set; }
    }
}