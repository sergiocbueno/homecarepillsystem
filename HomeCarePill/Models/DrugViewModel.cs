﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HomeCarePill.Models
{
    public class DrugViewModel
    {
        [Required(ErrorMessage = "Drug name is required.")]
        [Display(Name = "Drug Name:")]
        public string Name { get; set; }

        public List<string> DrugsName { get; set; }
    }
}