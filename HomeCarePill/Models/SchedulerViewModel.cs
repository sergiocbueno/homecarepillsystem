﻿using HomeCarePill.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace HomeCarePill.Models
{
    public class SchedulerViewModel
    {
        [Required]
        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public string Date { get; set; }

        [Required]
        [Display(Name = "Time:")]
        [DataType(DataType.Time)]
        public TimeSpan Time { get; set; }

        [Display(Name = "Status:")]
        public SchedulerSlotStatus Status { get; set; }
        
        [Display(Name = "Drug:")]
        public ICollection<SelectListItem> Drugs { get; set; }

        public string SelectedDrug { get; set; }
    }
}