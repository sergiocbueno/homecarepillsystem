﻿namespace HomeCarePill.Models
{
    public class AnalyserViewModel
    {
        public int Points { get; set; }

        public int Attempt { get; set; }
    }
}