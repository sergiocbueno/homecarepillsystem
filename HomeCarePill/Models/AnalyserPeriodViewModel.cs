﻿using System.ComponentModel.DataAnnotations;

namespace HomeCarePill.Models
{
    public class AnalyserPeriodViewModel
    {
        [Required]
        [Display(Name = "StartDate:")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public string StartDate { get; set; }

        [Required]
        [Display(Name = "EndDate:")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public string EndDate { get; set; }
    }
}