﻿using System.IO;
using System.IO.Ports;
using System.Net.Sockets;

namespace HomeCarePill.Services
{
    public class ComunicationService
    {
        public void Sender(string ip, int port, string message)
        {
            TcpClient TcpAc = new TcpClient(ip, port);
            NetworkStream ns = TcpAc.GetStream();
            StreamWriter sw = new StreamWriter(ns);
            sw.Write(message);
            sw.Flush();
            TcpAc.Close();
        }

        public string Receptor (string port)
        {
            SerialPort myPort = new SerialPort();
            myPort.BaudRate = 9600;
            myPort.PortName = port;
            myPort.Parity = Parity.None;
            myPort.StopBits = StopBits.One;
            myPort.Handshake = Handshake.None;
            myPort.DataBits = 8;

            var result = string.Empty;

            if (!myPort.IsOpen)
            {
                myPort.Open();
                result = myPort.ReadLine();
                result.Replace("\r", string.Empty);
                myPort.Close();
            }

            return result;
        }
    }
}