﻿using HomeCarePill.Database;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace HomeCarePill.Services
{
    public class DrugService
    {
        private ParameterizationEntities parameterization;

        public DrugService()
        {
            parameterization = new ParameterizationEntities();
        }

        public string Validate (string name)
        {
            if (name.Equals(string.Empty))
            {
                return "You are not allow to add a drug without name on system!";
            }

            var drug = parameterization.Drug.SingleOrDefault(x => x.Name.Equals(name));

            if(drug != null)
            {
                return "The system already has a drug with this name! [" + name + "]";
            }

            return string.Empty;
        }

        public void RegisterDrug (string name)
        {
            var drug = new Drug
            {
                Name = name.ToUpper(),
            };
            parameterization.Entry(drug).State = EntityState.Added;
            parameterization.SaveChanges();
        }

        public List<Drug> SearchDrug (string name)
        {
            var nameToUpper = name.ToUpper();
            var drugs = parameterization.Drug.ToList();

            var result = drugs.Where(x => x.Name.Contains(nameToUpper));
            return result.OrderBy(x => x.Name).ToList();
        }

        public List<SelectListItem> PopulateDropdownWithAllDrugsOnSystem()
        {
            var result = new List<SelectListItem>();
            var drugs = parameterization.Drug.ToList().OrderBy(x => x.Name);

            foreach(var drug in drugs)
            {
                var item = new SelectListItem
                {
                    Text = drug.Name,
                    Value = drug.Name
                };
                result.Add(item);
            }

            return result;
        }
    }
}