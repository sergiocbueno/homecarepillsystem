﻿using HomeCarePill.Enum;
using System.Media;

namespace HomeCarePill.Services
{
    public class AlertService
    {
        public void AlertAudio(AlertStatus status)
        {
            var alertSound = new SoundPlayer();//"~/Content/css

            switch (status)
            {
                case AlertStatus.FirstTime:
                    alertSound = new SoundPlayer(@"C:\Users\Sérgio Bueno\Documents\TCC\homecarepillsystem\HomeCarePill\Audios\DrugTime.wav");
                    break;
                case AlertStatus.SecondTime:
                    alertSound = new SoundPlayer(@"C:\Users\Sérgio Bueno\Documents\TCC\homecarepillsystem\HomeCarePill\Audios\DrugTimeAgain.wav");
                    break;
                case AlertStatus.LastTime:
                    alertSound = new SoundPlayer(@"C:\Users\Sérgio Bueno\Documents\TCC\homecarepillsystem\HomeCarePill\Audios\DrugTimeLast.wav");
                    break;
                case AlertStatus.Sucess:
                    alertSound = new SoundPlayer(@"C:\Users\Sérgio Bueno\Documents\TCC\homecarepillsystem\HomeCarePill\Audios\Sucess.wav");
                    break;
            }

            alertSound.Play();
        }
    }
}