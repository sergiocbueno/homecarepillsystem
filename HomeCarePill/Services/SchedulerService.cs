﻿using HomeCarePill.Database;
using HomeCarePill.Enum;
using HomeCarePill.Jobs;
using HomeCarePill.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace HomeCarePill.Services
{
    public class SchedulerService
    {
        private ParameterizationEntities parameterization;
        string ip = ConfigurationManager.AppSettings["HardwareIp"];
        int port = Convert.ToInt32(ConfigurationManager.AppSettings["HardwarePort"]);
        string usbPort = ConfigurationManager.AppSettings["UsbPort"];
        double acceptanceDistance = Convert.ToDouble(ConfigurationManager.AppSettings["AcceptanceDistance"]);

        public SchedulerService()
        {
            parameterization = new ParameterizationEntities();
        }

        public bool ValidateDates (List<DateTime> dates)
        {
            var datesQuantity = dates.Count;

            for (var i = 0; i < datesQuantity; i++)
            {
                for(var j = (i + 1); j < datesQuantity; j++)
                {
                    if ((dates[i] < DateTime.Now) || (dates[i] > dates[j]))
                        return false;
                }
            }

            return true;
        }

        public void SetUpMotorAndAlert (List<DateTime> dates)
        {
            foreach (var date in dates)
            {
                JobScheduler.SenderComunicationStart(ip, port, "M", date);
                JobScheduler.AudioAlertStart(AlertStatus.FirstTime, date.AddSeconds(5));

                JobScheduler.ReceptorComunicationStart(usbPort, date.AddSeconds(60), 1);
                JobScheduler.SenderComunicationStart(ip, port, "S", date.AddSeconds(65));
            }
        }

        public void VerifyDrugUsed (string distance, string repeat)
        {
            var distanceCalculated = Convert.ToDouble(distance);
            var repeatEnumValue = Convert.ToInt32(repeat);
            var type = (AlertStatus)repeatEnumValue;

            if (distanceCalculated <= acceptanceDistance)
            {
                switch (type)
                {
                    case AlertStatus.SecondTime:
                        JobScheduler.AudioAlertStart(AlertStatus.SecondTime, DateTime.Now.AddSeconds(5));
                        JobScheduler.ReceptorComunicationStart(usbPort, DateTime.Now.AddSeconds(60), 2);
                        JobScheduler.SenderComunicationStart(ip, port, "S", DateTime.Now.AddSeconds(65));
                        break;
                    case AlertStatus.LastTime:
                        JobScheduler.AudioAlertStart(AlertStatus.LastTime, DateTime.Now.AddSeconds(5));
                        JobScheduler.ReceptorComunicationStart(usbPort, DateTime.Now.AddSeconds(60), 3);
                        JobScheduler.SenderComunicationStart(ip, port, "S", DateTime.Now.AddSeconds(65));
                        break;
                    case AlertStatus.NotSucess:
                        UpdateStatusDrug(false);
                        break;
                    default:
                        throw new ArgumentException("Alert type not found!");
                }
                
            }
            else
            {
                JobScheduler.AudioAlertStart(AlertStatus.Sucess, DateTime.Now.AddSeconds(5));
                UpdateStatusDrug(true);
            }
        }
        
        public void SetUpSchedulerSlots(List<DateTime> dates, List<string> comments, string userMail)
        {
            for(int i = 0; i < dates.Count(); i++)
            {
                var slot = parameterization.Scheduler.First(x => x.Id.Equals(i+1));
                slot.Date = dates[i];
                slot.Status = (int)SchedulerSlotStatus.Waiting;
                slot.Comment = comments[i];
                slot.UserMail = userMail;
                parameterization.Entry(slot).State = EntityState.Modified;
            }

            parameterization.SaveChanges();
        }

        public List<SchedulerViewModel> LoadSchedulerSlots ()
        {
            var slots = parameterization.Scheduler.ToList();
            var drugs = parameterization.Drug.ToList();

            var schedulerViewModel = new List<SchedulerViewModel>();
            
            foreach (var slot in slots)
            {
                var dropdown = new List<SelectListItem>();
                foreach (var drug in drugs)
                {
                    var value = new SelectListItem();
                    value.Text = drug.Name;
                    value.Value = drug.Name;
                    if (drug.Name.Equals(slot.Comment))
                    {
                        value.Selected = true;
                    }
                    dropdown.Add(value);
                }

                var orderableDropdown = dropdown.OrderBy(x => x.Text).ToList();

                var item = new SchedulerViewModel
                {
                    Date = slot.Date.ToString("yyyy-MM-dd"),
                    Time = new TimeSpan(slot.Date.Hour, slot.Date.Minute, 00),
                    Status = (SchedulerSlotStatus)slot.Status,
                    Drugs = orderableDropdown,
                    SelectedDrug = slot.Comment
                };
                schedulerViewModel.Add(item);
            }

            return schedulerViewModel;
        }

        private void UpdateStatusDrug(bool used)
        {
            var slot = parameterization.Scheduler.First(x => x.Status.Equals((int)SchedulerSlotStatus.Waiting));
            var userMail = slot.UserMail;
            var comment = slot.Comment;
            var date = slot.Date;

            slot.Status = used ? (int)SchedulerSlotStatus.Used : (int)SchedulerSlotStatus.Unused;
            parameterization.Entry(slot).State = EntityState.Modified;
            parameterization.SaveChanges();

            if (!used)
            {
                var subject = "Paciente não tomou os medicamentos na hora correta!";
                var body = "Os remedios (" + comment + ") deveriam ter sido ingeridos as " + date.ToString() + " porém não foram!";
                JobScheduler.SendMailStart(userMail, subject, body);
            }
        }
    }
}