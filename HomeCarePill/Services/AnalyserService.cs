﻿using HomeCarePill.Database;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HomeCarePill.Services
{
    public class AnalyserService
    {
        private ParameterizationEntities parameterization;

        public AnalyserService()
        {
            parameterization = new ParameterizationEntities();
        }

        public void BackgroundAnalyserCustomer(bool used, int attempt)
        {
            var latestId = parameterization.Analyser.Max(x => x.Id);
            var lastAnalyse = parameterization.Analyser.First(x => x.Id.Equals(latestId));

            var newPoints = lastAnalyse.Points;
            var reason = string.Empty;

            if (used)
            {
                //Alright in first time
                if(attempt.Equals(1) && lastAnalyse.Attempt.Equals(1)) // E passado igual a primeira
                {
                    newPoints = newPoints + 3;
                    reason = "Used on the first attempt and last time too";
                }
                else if (attempt.Equals(1) && lastAnalyse.Attempt.Equals(2)) // E passado igual a segunda
                {
                    newPoints = newPoints + 2;
                    reason = "Used on the first attempt but last time on second attempt";
                }
                else if (attempt.Equals(1) && lastAnalyse.Attempt.Equals(3)) // E passado igual a terceira
                {
                    reason = "Used on the first attempt but last time on third attempt";
                    newPoints = newPoints + 1;
                }

                //Alright in second time
                if (attempt.Equals(2) && lastAnalyse.Attempt.Equals(1)) // E passado igual a primeira
                {
                    newPoints = newPoints + 2;
                    reason = "Used on the second attempt and last time on first attempt";
                }
                else if (attempt.Equals(2) && lastAnalyse.Attempt.Equals(2)) // E passado igual a segunda
                {
                    newPoints = newPoints + 1;
                    reason = "Used on the second attempt and last time too";
                }

                //Alright in third time
                if (attempt.Equals(3) && lastAnalyse.Attempt.Equals(1)) // E passado igual a primeira
                {
                    newPoints = newPoints + 1;
                    reason = "Used on the third attempt and last time on first attempt";
                }
            }
            else
            {
                if(lastAnalyse.Attempt.Equals(4)) // passado nao tomado
                {
                    var pastId = latestId > 1 ? latestId - 1: latestId;
                    var pastAnalyse = parameterization.Analyser.First(x => x.Id.Equals(pastId));
                    var different = lastAnalyse.Points - pastAnalyse.Points;

                    newPoints = (different * 2) + newPoints; //passado 2x
                    reason = "Unused and last time too";
                }
                else if (lastAnalyse.Attempt.Equals(1))
                {
                    newPoints = newPoints - 1;
                    reason = "Unused but last time used on first attempt";
                }
                else if (lastAnalyse.Attempt.Equals(2))
                {
                    newPoints = newPoints - 2;
                    reason = "Unused but last time used on second attempt";
                }
                else if (lastAnalyse.Attempt.Equals(3))
                {
                    newPoints = newPoints - 3;
                    reason = "Unused but last time used on third attempt";
                }

            }

            var newAnalyser = new Analyser
            {
                Points = newPoints,
                Attempt = attempt,
                Date = DateTime.Now,
                Reason = reason,
                User = "System"
            };

            parameterization.Analyser.Add(newAnalyser);
            parameterization.SaveChanges();
        }

        public List<Analyser> GetInformationByDrugAndPeriod(string drug, DateTime startDate, DateTime endDate)
        {
            var analysers = parameterization.Analyser.Where(x => x.Date >= startDate && x.Date <= endDate);

            return analysers.ToList();
        }

        public List<Analyser> GetInformationByPeriod(DateTime startDate, DateTime endDate)
        {
            var analysers = parameterization.Analyser.Where(x => x.Date >= startDate && x.Date <= endDate);

            return analysers.ToList();
        }
    }
}