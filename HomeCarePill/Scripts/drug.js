﻿
window.onload = function () {

    document.getElementById('addDrug').onclick = function () {
        
        var drugName = $('#drugName').val();

        addDrug(drugName);
    }

}

function addDrug(drugName) {
    $.ajax({
        type: "POST",
        data: { name: drugName },
        url: '/Home/AddDrug',
        success: function (response) {
        if (response != null && response.success) {
            window.location.href = response.responseText;
        } else {
            $('#error-message').text(response.responseText);
        }                          
    }
    });
}