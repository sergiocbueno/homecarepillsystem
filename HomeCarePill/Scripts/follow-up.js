﻿var information = [];
var diffDays = 0;

window.onload = function () {

    document.getElementById('search').onclick = function () {

        var dropdown = $('#dropdown').val();
        var startDate = $('#StartDate').val();
        var endDate = $('#EndDate').val();

        var start = new Date(startDate);
        var end = new Date(endDate);
        diffDays = end.getDate() - start.getDate();

        getChartInformation(dropdown, startDate, endDate);
    }

}

function getChartInformation(dropdown, startDate, endDate) {
    $.ajax({
        type: "POST",
        data: { drug: dropdown, startDate: startDate, endDate: endDate },
        url: '/Home/FollowUpChartInformation',
        success: function (response) {
            if (response != null && response.success) {
                $('#error-message').hide();
                information = [];
                var quantity = response.data.length;

                for(i=0; i< quantity; i++){
                    var item = [new Date(response.data[i].Year, response.data[i].Month, response.data[i].Day), response.data[i].Used]
                    information.push(item);
                }

                google.charts.load('current', { 'packages': ['corechart'] });
                google.charts.setOnLoadCallback(drawChart);
                $('#chart_div').show();
            } else {
                $('#chart_div').hide();
                $('#error-message').text(response.responseText);
                $('#error-message').show();
            }
        }
    });
}



function drawChart() {

    var data = new google.visualization.DataTable();
    data.addColumn('date', 'Time of Day');
    data.addColumn('number', 'Used');

    data.addRows(information);

    var options = {
        title: 'Chart on scale of 0 (Unsed) to 1 (Used)',
        width: 900,
        height: 500,
        hAxis: {
            format: 'dd/MM/yyyy',
            gridlines: { count: diffDays }
        },
        vAxis: {
            gridlines: { color: 'none' },
            minValue: 0
        }
    };

    var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
    chart.draw(data, options);
}
