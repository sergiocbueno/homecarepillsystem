﻿var information = [['Points', 'Attemps']];

window.onload = function () {

    document.getElementById('search').onclick = function () {

        var startDate = $('#StartDate').val();
        var endDate = $('#EndDate').val();

        getChartInformation(startDate, endDate);
    }

}

function getChartInformation(startDate, endDate) {
    $.ajax({
        type: "POST",
        data: { startDate: startDate, endDate: endDate },
        url: '/Home/AnalyserValues',
        success: function (response) {
            if (response != null && response.success) {
                $('#error-message').hide();
                information = [['Points', 'Attemps']];
                var quantity = response.data.length;

                for (i = 0; i < quantity; i++) {
                    var item = [response.data[i].Point, response.data[i].Attemp]
                    information.push(item);
                }

                google.charts.load('current', { 'packages': ['corechart'] });
                google.charts.setOnLoadCallback(drawChart);
                $('#chart_div').show();
            } else {
                $('#chart_div').hide();
                $('#error-message').text(response.responseText);
                $('#error-message').show();
            }
        }
    });
}

function drawChart() {
    var data = google.visualization.arrayToDataTable(information);

    var options = {
        title: 'Points vs. attemps comparison',
        width: 900,
        height: 500,
        hAxis: { title: 'Points', minValue: 0, maxValue: 50 },
        vAxis: { title: 'Attemps', minValue: 0, maxValue: 50 },
        legend: 'none'
    };

    var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));
    chart.draw(data, options);
}