﻿
window.onload = function () {

    document.getElementById('saveChanges').onclick = function () {
        
        var dates = [];
        var comments = [];

        for (var i = 0; i < 6; i++) {

            var date = document.getElementsByName("[" + i + "].Date")[0].value;
            var time = document.getElementsByName("[" + i + "].Time")[0].value;
            var datetime = date + " " + time;
            
            var comment = $('#dropdown'+i).val();

            dates.push(datetime);
            comments.push(comment);
        }

        saveConfigurations(dates, comments);
    }

}

function saveConfigurations(dates, comments) {
    $.ajax({
        type: "POST",
        data: { dates: dates, comments: comments },
        url: '/Home/UpdateScheduler',
        success: function (response) {
        if (response != null && response.success) {
            window.location.href = response.responseText;
        } else {
            $('#error-message').text(response.responseText);
        }                          
    }
    });
}