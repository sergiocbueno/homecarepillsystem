﻿using System;
using System.Web.Mvc;
using System.Collections.Generic;
using HomeCarePill.Services;
using HomeCarePill.Models;
using System.Linq;

namespace HomeCarePill.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Title = "About";
            ViewBag.Message = "Version 2.7";
            
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Title = "Contact";
            ViewBag.Message = "Pontifícia Universidade Católica de Campinas (PUC-CAMPINAS).";

            return View();
        }

        [Authorize]
        public ActionResult Analyser()
        {
            const string analyser = "Analyser";
            var today = DateTime.Today;

            ViewBag.Title = analyser;
            ViewBag.Message = "Patient analysis by period";

            var analyserPeriodViewModel = new AnalyserPeriodViewModel
            {
                StartDate = today.ToString("yyyy-MM-dd"),
                EndDate = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month)).ToString("yyyy-MM-dd")
            };

            return View(analyser, analyserPeriodViewModel);
        }

        [Authorize]
        public ActionResult Scheduler()
        {
            const string scheduler = "Scheduler";
            var schedulerService = new SchedulerService();

            ViewBag.Title = scheduler;
            ViewBag.Message = "Please set up date and time of each slot.";

            var schedulerViewModel = schedulerService.LoadSchedulerSlots();

            return View(scheduler, schedulerViewModel);
        }

        [Authorize]
        public ActionResult Drug()
        {
            return View("Drug", new DrugViewModel());
        }

        [Authorize]
        public ActionResult FollowUp()
        {
            const string followUp = "FollowUp";
            var today = DateTime.Today;
            var drugService = new DrugService();

            ViewBag.Title = "Follow-up";
            ViewBag.Message = "Follow-up your patient by drug.";

            var followUpViewModel = new FollowUpViewModel
            {
                StartDate = today.ToString("yyyy-MM-dd"),
                EndDate = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month)).ToString("yyyy-MM-dd"),
                Drugs = drugService.PopulateDropdownWithAllDrugsOnSystem()
            };

            return View(followUp, followUpViewModel);
        }

        [HttpPost]
        public JsonResult UpdateScheduler(List<DateTime> dates, List<string> comments)
        {
            var schedulerService = new SchedulerService();
            var validate = schedulerService.ValidateDates(dates);

            if(!validate)
            {
                var errorMessage = String.Format("Invalid date: You try to save a date in the past! Remember, slots need to have sequence dates!");
                return Json(new { success = false, responseText = errorMessage }, JsonRequestBehavior.AllowGet);
            }

            var user = User.Identity.Name;
            schedulerService.SetUpMotorAndAlert(dates);
            schedulerService.SetUpSchedulerSlots(dates, comments, user);

            return Json(new { success = true, responseText = HttpContext.Request.UrlReferrer.AbsoluteUri }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AnalyserValues(DateTime startDate, DateTime endDate)
        {
            var analyserService = new AnalyserService();
            var analysers = analyserService.GetInformationByPeriod(startDate, endDate);

            if (analysers == null || !analysers.Any())
            {
                return Json(new { success = false, responseText = "System doesn't have information of this drug for this period!" }, JsonRequestBehavior.AllowGet);
            }

            List<object> result = new List<object>();
            foreach (var item in analysers)
            {
                result.Add(new { Point = item.Points, Attemp = item.Attempt });
            }

            return Json(new { success = true, data = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult FindDrug (string name)
        {
            var drugService = new DrugService();
            var result = drugService.SearchDrug(name).Select(x => x.Name).ToList();

            var drugViewModel = new DrugViewModel
            {
                Name = name,
                DrugsName = result
            };

            return View("Drug", drugViewModel);
        }

        [HttpPost]
        public JsonResult AddDrug(string name)
        {
            var drugService = new DrugService();
            var validate = drugService.Validate(name);

            if (!validate.Equals(string.Empty))
            {
                return Json(new { success = false, responseText = validate }, JsonRequestBehavior.AllowGet);
            }

            drugService.RegisterDrug(name);

            return Json(new { success = true, responseText = HttpContext.Request.UrlReferrer.AbsoluteUri }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FollowUpChartInformation(string drug, DateTime startDate, DateTime endDate)
        {
            var analyserService = new AnalyserService();
            var analysers = analyserService.GetInformationByDrugAndPeriod(drug, startDate, endDate);

            if (analysers == null || !analysers.Any())
            {
                return Json(new { success = false, responseText = "System doesn't have information of this drug for this period!" }, JsonRequestBehavior.AllowGet);
            }

            List<object> result = new List<object>();
            foreach(var item in analysers)
            {
                result.Add(new { Year = item.Date.Year, Month = item.Date.Month, Day = item.Date.Day, Used = item.Attempt.Equals(4) ? 0 : 1 });
            }

            return Json(new { success = true, data = result }, JsonRequestBehavior.AllowGet);
        }
    }
}