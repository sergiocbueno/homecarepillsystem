﻿namespace HomeCarePill.Enum
{
    public enum SchedulerSlotStatus
    {
        Waiting,
        Used,
        Unused
    }
}