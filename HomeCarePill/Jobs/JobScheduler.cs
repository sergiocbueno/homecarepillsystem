﻿using HomeCarePill.Enum;
using Quartz;
using Quartz.Impl;
using System;

namespace HomeCarePill.Jobs
{
    public class JobScheduler
    {
        public static void ReceptorComunicationStart(string port, DateTime time, int repeat)
        {
            var scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            var job = JobBuilder.Create<ReceptorComunicationJob>()
                .WithIdentity("ReceptorComunication-Date: " + time.ToString())
                .UsingJobData("port", port)
                .UsingJobData("repeat", repeat)
                .Build();

            var trigger = TriggerBuilder.Create()
            .WithIdentity("ReceptorComunication-Date: " + time.ToString())
            .StartAt(time)
            .WithPriority(2)
            .Build();

            scheduler.ScheduleJob(job, trigger);
        }

        public static void SenderComunicationStart(string ip, int port, string message, DateTime time)
        {
            var scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            var job = JobBuilder.Create<SenderComunicationJob>()
                .WithIdentity("SenderComunication-Date: " + time.ToString())
                .UsingJobData("ip", ip)
                .UsingJobData("port", port)
                .UsingJobData("message", message)
                .Build();

            var trigger = TriggerBuilder.Create()
            .WithIdentity("SenderComunication-Date: " + time.ToString())
            .StartAt(time)
            .WithPriority(1)
            .Build();

            scheduler.ScheduleJob(job, trigger);
        }

        public static void AudioAlertStart(AlertStatus type, DateTime time)
        {
            var scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            var job = JobBuilder.Create<AudioAlertJob>()
                .WithIdentity("AudioAlert-Date: " + time.ToString())
                .UsingJobData("alertType", (int)type)
                .Build();

            var trigger = TriggerBuilder.Create()
            .WithIdentity("AudioAlert-Date: " + time.ToString())
            .StartAt(time)
            .WithPriority(2)
            .Build();

            scheduler.ScheduleJob(job, trigger);
        }

        public static void SendMailStart(string mail, string subject, string body)
        {
            var scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            var job = JobBuilder.Create<SendMailJob>()
                .WithIdentity("SendMail")
                .UsingJobData("userMail", mail)
                .UsingJobData("subject", subject)
                .UsingJobData("body", body)
                .Build();

            var trigger = TriggerBuilder
                .Create()
                .WithIdentity("SendMail")
                .WithPriority(5)
                .StartNow()
                .Build();

            scheduler.ScheduleJob(job, trigger);
        }
    }
}
