﻿using System;
using Quartz;
using HomeCarePill.Services;

namespace HomeCarePill.Jobs
{
    public class SenderComunicationJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var comunication = new ComunicationService();

            var dataMap = context.JobDetail.JobDataMap;
            var ip = dataMap["ip"].ToString();
            var port = Convert.ToInt32(dataMap["port"]);
            var message = dataMap["message"].ToString();

            comunication.Sender(ip, port, message);
        }
    }
}