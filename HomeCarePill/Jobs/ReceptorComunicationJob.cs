﻿using System;
using Quartz;
using HomeCarePill.Services;

namespace HomeCarePill.Jobs
{
    public class ReceptorComunicationJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var comunication = new ComunicationService();
            var schedulerService = new SchedulerService();

            var dataMap = context.JobDetail.JobDataMap;
            var port = dataMap["port"].ToString();
            var repeat = dataMap["repeat"].ToString();

            var distance = comunication.Receptor(port);
            schedulerService.VerifyDrugUsed(distance, repeat);
        }
    }
}