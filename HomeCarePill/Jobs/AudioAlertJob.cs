﻿using Quartz;
using HomeCarePill.Services;
using HomeCarePill.Enum;
using System;

namespace HomeCarePill.Jobs
{
    public class AudioAlertJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var alert = new AlertService();

            var dataMap = context.JobDetail.JobDataMap;
            var alertType = Convert.ToInt32(dataMap["alertType"]);
            var type = (AlertStatus)alertType;
            
            alert.AlertAudio(type);
        }
    }

}