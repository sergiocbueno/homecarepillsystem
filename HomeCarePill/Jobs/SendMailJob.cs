﻿using Quartz;
using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace HomeCarePill.Jobs
{
    public class SendMailJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var dataMap = context.JobDetail.JobDataMap;
            var userMail = dataMap["userMail"].ToString();
            var subject = dataMap["subject"].ToString();
            var body = dataMap["body"].ToString();

            using (var message = new MailMessage(ConfigurationManager.AppSettings["SmtpUserName"], userMail))
            {
                message.Subject = subject;
                message.Body = body;

                using (SmtpClient client = new SmtpClient
                {
                    EnableSsl = true,
                    Host = "smtp.gmail.com",
                    Port = 587,
                    Credentials = new NetworkCredential(ConfigurationManager.AppSettings["SmtpUserName"], ConfigurationManager.AppSettings["SmtpUserPassword"])
                })
                {
                    client.Send(message);
                }
            }
        }
    }
}